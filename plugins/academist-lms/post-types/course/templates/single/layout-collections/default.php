<div class="eltdf-course-single-wrapper">
	<div class="eltdf-course-title-wrapper">
		<div class="eltdf-course-left-section">
			<?php academist_lms_get_cpt_single_module_template_part( 'single/parts/title', 'course', '', $params ); ?>
			<?php academist_lms_get_cpt_single_module_template_part( 'single/parts/course-type', 'course', '', $params ); ?>
		</div>
		<div class="eltdf-course-right-section">
            <span class="eltdf-course-whishlist-wrapper">
			    <?php if ( function_exists( 'academist_membership_get_favorite_template' ) ) {
				    academist_membership_get_favorite_template(get_the_ID(), 'icon-with-text');
			    } ?>
            </span>
		</div>
	</div>
	<div class="eltdf-course-basic-info-wrapper">
		<div class="eltdf-grid-row">
			<div class="eltdf-grid-col-9">
				<div class="eltdf-grid-row">
					
					<div class="eltdf-grid-col-4">
						<div class="eltdf-course-categories">
							<div class="eltdf-course-category-label">
								<b>Início:</b>
							</div>
							<div class="eltdf-course-info-inicio">
								<?php 
									#Verifica campo de ACF para personalizar informação do curso
									if (function_exists('get_field') && get_field('inicio_do_curso', get_the_ID())) {
										echo get_field('inicio_do_curso', get_the_ID());
									} else {
										echo '<i>à definir</i>';
									}
								?>
							</div>
						</div>
					</div>
					<?php #academist_lms_get_cpt_single_module_template_part( 'single/parts/instructor', 'course', '', $params ); ?>
					
					<?php academist_lms_get_cpt_single_module_template_part( 'single/parts/categories', 'course', '', $params ); ?>
					<?php academist_lms_get_cpt_single_module_template_part( 'single/parts/reviews', 'course', '', $params ); ?>
				</div>
			</div>
			<div class="eltdf-grid-col-3">
				<?php academist_lms_get_cpt_single_module_template_part( 'single/parts/action', 'course', '', $params ); ?>
			</div>
		</div>
	</div>
	<?php /*
	#Imagem destacada ocultada
	<div class="eltdf-course-image-wrapper">
		<?php academist_lms_get_cpt_single_module_template_part( 'single/parts/image', 'course', '', $params ); ?>
	</div>
	*/ ?>
	<div class="eltdf-course-tabs-wrapper">
		<?php academist_lms_get_cpt_single_module_template_part( 'single/parts/tabs', 'course', '', $params ); ?>
	</div>
    <?php academist_lms_get_cpt_single_module_template_part( 'single/parts/social', 'course', '', $params ); ?>
</div>