<?php /*if ( comments_open() ) { ?>
#Avaliações fechadas
	<div class="eltdf-grid-col-4">
		<div class="eltdf-course-reviews">
			<div class="eltdf-course-reviews-label">
				<?php esc_html_e( 'Reviews:', 'academist-lms' ) ?>
			</div>
			<span class="eltdf-course-stars">
	            <?php
                if(academist_lms_core_plugin_installed()) {
                    echo academist_core_list_review_details('stars', array('rating_label', 'rating_number'));
                }
                ?>
			</span>
		</div>
	</div>
<?php }*/ ?>
	<div class="eltdf-grid-col-4">
		<div class="eltdf-course-reviews">
			<div class="eltdf-course-reviews-label">
				<b>Valor:</b>
			</div>
			<span class="eltdf-course-stars">
				<?php
				
				$valor_total = academist_lms_calculate_course_price( get_the_ID() );
				$parcelas = 3;
				$avista = $valor_total*0.9;
				if (isset($valor_total) && !empty($valor_total)) {
					if (function_exists('get_field')) {
						$parcelas = get_field('parcelamento', get_the_ID()) ? get_field('parcelamento', get_the_ID()) : 3;
						$valor = (float)$valor_total / (int)$parcelas;
						if ($parcelas == '1') {
							echo 'À vista: <b class="color-primary">'.$parcelas.'x de R$'.number_format($valor,2,",",".").'</b><small style="display: block;">*10% de desconto</small>';
						} else {
							echo 'Parcelado: <b class="color-primary">'.$parcelas.'x de R$'.number_format($valor,2,",",".").'</b>';
							echo '<br>À vista: <b>R$'.number_format($avista,2,",",".").'</b><small style="display: block;">*10% de desconto</small>';
						}
					} else {
						$valor = (float)$valor_total / (int)$parcelas;
						echo '<b class="color-primary">'.$parcelas.'x de R$'.number_format($valor,2,",",".").'</b>';
						echo '<br>À vista: <b>R$'.number_format($avista,2,",",".").'</b><small style="display: block;">*10% de desconto</small>';
					}
				} else {
					echo 'Não informado';
				}
                ?>
			</span>
		</div>
	</div>