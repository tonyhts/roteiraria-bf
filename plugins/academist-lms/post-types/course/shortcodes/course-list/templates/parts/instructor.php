<?php
$instructor = get_post_meta( get_the_ID(), 'eltdf_course_instructor_meta', true );
?>
<a itemprop="url" href="<?php echo get_permalink( $instructor ); ?>">
    <span class="eltdf-instructor-name">
       <?php #echo get_the_title( $instructor ); ?>
       <?php 
            #Adiciona mais de 1 professor na listagem
            #Verifica campo de ACF para personalizar informação do curso
            if (function_exists('get_field') && get_field('professores_curso', get_the_ID())) {
                $professores = get_field('professores_curso', get_the_ID());
                foreach ($professores as $key => $professor) {
                    if ($key != 0) {
                        echo ' & ';
                    }
                    echo get_the_title($professor);
                }
            } else {
                echo get_the_title( $instructor );
            }
        ?>
    </span>
</a>