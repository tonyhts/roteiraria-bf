<?php if ( $excerpt_length !== '0' && $excerpt_length !== '' && $enable_excerpt === 'yes' ) {
	$excerpt = ( $excerpt_length > 0 ) ? substr( get_the_excerpt(), 0, intval( $excerpt_length ) ) : get_the_excerpt();
	?>
	<p itemprop="description" class="eltdf-cli-excerpt"><?php echo get_the_excerpt(); ?></p>
<?php } ?>

<?php 
#Retorna barra de info do curso na listagem
echo '<div class="info-curso">';
	#Verifica campo de ACF para personalizar informação do curso
	if (function_exists('get_field') && get_field('inicio_do_curso', get_the_ID())) {
		echo '<span><b>Início:</b> '.get_field('inicio_do_curso', get_the_ID()).'</span>';
	} else {
		echo '<span><b>Início:</b> <i>à definir</i></span>';
	}
	echo '<a href="'.get_the_permalink().'">Saiba Mais</a>';
echo '</div>';
?>