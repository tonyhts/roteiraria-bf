<div class="eltdf-course-list-holder eltdf-grid-list eltdf-disable-bottom-space clearfix <?php echo esc_attr( $holder_classes ); ?>" <?php echo wp_kses( $holder_data, array( 'data' ) ); ?>>
	<?php echo academist_lms_get_cpt_shortcode_module_template_part( 'course', 'course-list', 'filters/filter', '', $params, $additional_params ); ?>
	<?php echo academist_lms_get_cpt_shortcode_module_template_part( 'course', 'course-list', 'parts/filter', '', $params, $additional_params ); ?>
	<div class="eltdf-cl-inner eltdf-outer-space <?php echo esc_attr( $holder_inner_classes ); ?>">
		<?php

		#Verifica se o post irá repetir na página de cursos
		$pos = strpos($_SERVER["REQUEST_URI"], '/curso/');
		if ($pos === false) {
			$id_atual = 0;
		} else {
			$id_atual = get_the_ID();
		}

		if ( $query_results->have_posts() /*&& $filtro*/ ):
			while ( $query_results->have_posts() ) : 
				$query_results->the_post();
				if ($id_atual != get_the_ID()) {
					echo academist_lms_get_cpt_shortcode_module_template_part( 'course', 'course-list', 'course-item', $slug, $params );
				}
			endwhile;
		else:
			echo '<p style="padding: 0 15px;">Sem cursos no momento!</p>';
			#echo academist_lms_get_cpt_shortcode_module_template_part( 'course', 'course-list', 'parts/posts-not-found' );
		endif;
		
		wp_reset_postdata();
		?>
	</div>
	
	<?php echo academist_lms_get_cpt_shortcode_module_template_part( 'course', 'course-list', 'pagination/' . $pagination_type, '', $params, $additional_params ); ?>
</div>