<script type="text/javascript">
    jQuery(function(){
        jQuery('a[href^="#"]').on('click', function(event) {
            var target = jQuery(jQuery(this).attr('href'));
            if( target.length ) {
                event.preventDefault();
                jQuery('html, body').animate({
                    scrollTop: target.offset().top-80
                }, 500);
            }
            
        });
    });
</script>

<?php do_action( 'academist_elated_get_footer_template' );