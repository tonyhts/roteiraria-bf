<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
	<?php
	/**
	 * academist_elated_action_header_meta hook
	 *
	 * @see academist_elated_header_meta() - hooked with 10
	 * @see academist_elated_user_scalable_meta - hooked with 10
	 * @see academist_core_set_open_graph_meta - hooked with 10
	 */
	do_action( 'academist_elated_action_header_meta' );
	
	wp_head(); ?>

	<link rel="stylesheet" href="https://use.typekit.net/you6jdu.css">

		
		<!-- Google Tag Manager -->
	<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
	new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
	j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
	'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
	})(window,document,'script','dataLayer','GTM-TL6JL9TL');</script>
	<!-- End Google Tag Manager -->

	<!-- Facebook Pixel Code 
	<script>
	!function(f,b,e,v,n,t,s)
	{if(f.fbq)return;n=f.fbq=function(){n.callMethod?
	n.callMethod.apply(n,arguments):n.queue.push(arguments)};
	if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
	n.queue=[];t=b.createElement(e);t.async=!0;
	t.src=v;s=b.getElementsByTagName(e)[0];
	s.parentNode.insertBefore(t,s)}(window,document,'script',
	'https://connect.facebook.net/en_US/fbevents.js');
	fbq('init', '400086053958654'); 
	fbq('track', 'PageView');
	</script>
	<noscript>
	<img height="1" width="1" 
	src="https://www.facebook.com/tr?id=400086053958654&ev=PageView
	&noscript=1"/>
	</noscript>
	<!-- End Facebook Pixel Code -->
	
	<!-- Facebook Pixel Code Atualização a pedido da Vivi 25/11/21-->
	<script>
	!function(f,b,e,v,n,t,s)
	{if(f.fbq)return;n=f.fbq=function(){n.callMethod?
	n.callMethod.apply(n,arguments):n.queue.push(arguments)};
	if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
	n.queue=[];t=b.createElement(e);t.async=!0;
	t.src=v;s=b.getElementsByTagName(e)[0];
	s.parentNode.insertBefore(t,s)}(window,document,'script',
	'https://connect.facebook.net/en_US/fbevents.js');
	 fbq('init', '438148461027390'); 
	fbq('track', 'PageView');
	</script>
	<noscript>
	 <img height="1" width="1" 
	src="https://www.facebook.com/tr?id=438148461027390&ev=PageView
	&noscript=1"/>
	</noscript>
	<!-- End Facebook Pixel Code -->

</head>
<body <?php body_class(); ?> itemscope itemtype="http://schema.org/WebPage">
	<?php
	/**
	 * academist_elated_action_after_body_tag hook
	 *
	 * @see academist_elated_get_side_area() - hooked with 10
	 * @see academist_elated_smooth_page_transitions() - hooked with 10
	 */
	do_action( 'academist_elated_action_after_body_tag' ); ?>

		<!-- Google Tag Manager (noscript) -->
	<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-TL6JL9TL"
	height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
	<!-- End Google Tag Manager (noscript) -->

	<?php if(is_page(38)) { ?>
		<div class="menu-fixed">
			<a href="/eventos" class="i-agenda">
				<span class="icon-custom eltdf-icon-element dripicons-calendar"></span>
				<span>Agenda</span>
			</a>
			<a href="/cursos" class="i-cursos">
				<span aria-hidden="true" class="icon-custom eltdf-icon-font-elegant icon_menu-square_alt2 "></span>
				<span>Cursos</span>
			</a>
			<a href="#news">
				<span>Newsletter</span>
			</a>
		</div>
	<?php } ?>
	<style>
.whatsapp {
    position: fixed;
    top: 82%;
    right: 1%;
    padding: 10px;
    z-index: 10000000;
}
</style>
<div>
    <a href="https://api.whatsapp.com/send?phone=5511961685919" 
       target="_blank">
       <img  class="whatsapp" src="https://assets.boxloja.io/shop/img/whatsapp.png" />
    </a>
</div>

    <div class="eltdf-wrapper">
        <div class="eltdf-wrapper-inner">
            <?php
            /**
             * academist_elated_action_after_wrapper_inner hook
             *
             * @see academist_elated_get_header() - hooked with 10
             * @see academist_elated_get_mobile_header() - hooked with 20
             * @see academist_elated_back_to_top_button() - hooked with 30
             * @see academist_elated_get_header_minimal_full_screen_menu() - hooked with 40
             * @see academist_elated_get_header_bottom_navigation() - hooked with 40
             */
            do_action( 'academist_elated_action_after_wrapper_inner' ); ?>
	        
            <div class="eltdf-content" <?php academist_elated_content_elem_style_attr(); ?>>
                <div class="eltdf-content-inner">